// m_11.cpp
//

#include <iostream>

/*
* 
* print() function just for fun
* 
*/

void print()
{
    std::cout << "Hello Skillbox!\n";
}

int main()
{
    int a = 10;
    int b = 5;

    int c = a + b;

    print();
}

